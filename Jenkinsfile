#!/usr/bin/env groovy
@Library('jenkins-shared-library')

def groovy

pipeline {
    agent any

    tools {
      maven 'Maven'
    }

    stages {
        
        stage('Increment App Version') {
            steps {
                script {
                    echo "Incrementing Application Version....."
                    sh 'mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                        versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'       //here we get the version of app from pom.xml 
                    def version = matcher[0][1]
                    env.IMAGE_VERSION= "$version-$BUILD_NUMBER"                           //here we add build_number to image anme
                }
            }
        }
        stage ('Init Groovy') {
            steps {
                script {
                    groovy = load "script.groovy"
                }
            }
        }
        stage('Build Jar') {
            steps {
                script {
                   build_jar()
            }
          }
        }

        stage('Build Image') {
            steps {
               script {

                   env.IMAGE_NAME = "139646/java-maven-app:$IMAGE_VERSION"    //Using IMAGE_NAME to use the version of the app + build_number
                   build_image( "$IMAGE_NAME")      
               }
            }
        }

        stage("Provision EC2 Server") {
            //Provision EC2 Instance Using Terraform
            environment {
                AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
                AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
                TF_VAR_env_prefix = "test"                                               //overwrite the value "dev" 
            }
            steps {
                script {
                    dir('terraform') {
                        sh "terraform init"
                        sh "terraform apply --auto-approve"
                        //gives me this specific output which has the public ip of ec2 and puts it in this env var
                        env.EC2_PUBLIC_IP = sh(
                            script: "terraform output myapp-ec2-public-ip",             
                            returnStdout: true                                          //these two lines save the output and return it and save it in the env var
                        ).trim()                                                 
                        
                    }
                }
            }
            
        }

        stage ('Deploy') {
            steps {
                script {
                   
                   echo "Waiting for EC2 Server to Initialize ......"
                   //wait till the ec2 server is fully ready 
                   sleep(time: 150, unit: "SECONDS")
                   
                   echo "Deploying the App to EC2 ....."
                   echo "Public_IP_of_EC2-SERVER: ${EC2_PUBLIC_IP}"

                   def deckerComposeCMD = "bash ./server-script.sh $IMAGE_NAME"
                   def ec2Instance = "ubuntu@${EC2_PUBLIC_IP}"

                   sshagent(['complete-cicd-key-pair']) {

                       sh "scp -o StrictHostKeyChecking=no server-script.sh ${ec2Instance}:/home/ubuntu"
                       sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ubuntu"
                       sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${deckerComposeCMD}"
                   }
                   echo "Image deployed to EC2 successfuly!"
                }
            }
        }

        stage('commit version update') {
            steps {
                script {
                     withCredentials([usernamePassword(credentialsId: 'GitLab_Tokens', usernameVariable: 'USER', passwordVariable: 'PASSWORD')]) {
                          //set configurations 
                          sh 'git config --global user.email "mohmd46alaa@gmail.com"'
                          sh 'git config --global user.name "mohamedalaa13"'
                          sh 'git config -l '
                          //Using the URL below to make git Commands on
                          sh "git remote set-url origin https://${USER}:${PASSWORD}@gitlab.com/mohamedalaa13/complete_cicd.git"
                          sh 'git add .'
                          sh 'git commit -m "ci: version bump"'
                          sh 'git push origin HEAD:main'
                     }
                }
            }
        }
    }
}
